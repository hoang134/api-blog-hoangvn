<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        dd(JWTAuth::parseToken('eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9')->authenticate());
        if (Auth::check())
        return $next($request);

        return redirect()->route('home');
    }


}
